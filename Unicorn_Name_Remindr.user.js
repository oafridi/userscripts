// ==UserScript==
// @name		Unicorn Name Remindr
// @namespace	atlassian.userscripts.SAC
// @include		https://support.atlassian.com/browse/*
// @author		Brad Mallow
// @description	This userscript adds a useful reminder to messaging modal windows of the reporter's name.
// ==/UserScript==

// inject the script into the page to take advantage of the already present` jQuery 
function scriptLoader(callback) {
	var script = document.createElement("script");
	script.textContent = "(" + callback.toString() + ")();";
	document.body.appendChild(script);
}

// the guts of this userscript
function mainStage() {

	// HSW (Hydra Scripts Wranglr) Version Info
	var currentVersion = 1.2;
	jQuery('body').append('<div class="HSWversionInfoTag" scriptname="Unicorn_Name_Remindr" version="'+currentVersion+'" style="display: none;" />');
	
	function UNRagressiveAsker() {
		if (!jQuery('.jira-dialog-content .wiki-edit').length) {			
			setTimeout(UNRagressiveAsker, 500);
		} else {
			function UNRagressiveAskerToo() {
				if (jQuery('.jira-dialog-content .wiki-edit').length) {			
					setTimeout(UNRagressiveAskerToo, 500);
				} else {
					UNRagressiveAsker();
				}
			}
			jQuery('.jira-dialog-content .wiki-edit').parent().before('<div class="field-group"><label for="UNRcustomerNameField">Reporter</label><input type="text" value="'+jQuery('#reporter-val .user-hover').text().trim()+'" maxlength="254" name="UNRcustomerNameField" id="UNRcustomerNameField" class="textfield text" /></div>');
			UNRagressiveAskerToo();
		}
	}
	UNRagressiveAsker();
}

// engage!!
scriptLoader(mainStage);