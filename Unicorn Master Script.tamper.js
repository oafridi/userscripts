// ==UserScript==
// @name		Unicorn Master Script 
// @namespace	atlassian.userscripts.SAC
// @include		https://support.atlassian.com/browse/*
// @author		Osman
// @description	This userscript adds comments, description tab and users signature
// ==/UserScript==

// inject the script into the page to take advantage of the already present` jQuery 
function scriptLoader(callback) {
	var script = document.createElement("script");
	script.textContent = "(" + callback.toString() + ")();";
	document.body.appendChild(script);
}

// the guts of this userscript
function mainStage() {

	// HSW (Hydra Scripts Wranglr) Version Info
	var currentVersion = 1.2;
	jQuery('body').append('<div class="HSWversionInfoTag" scriptname="Unicorn_master_script" version="'+currentVersion+'" style="display: none;" />');
	
	function UNRagressiveAsker() {
		if (!jQuery('.jira-dialog-content .wiki-edit').length) {			
			setTimeout(UNRagressiveAsker, 500);
		} else {
			function UNRagressiveAskerToo() {
				if (jQuery('.jira-dialog-content .wiki-edit').length) {			
					setTimeout(UNRagressiveAskerToo, 500);
				} else {
					UNRagressiveAsker();
				}
			}
            
            // Comments
			jQuery('.jira-dialog-content .wiki-edit').parent().before('<div class="field-group toggle"><label for="UNRcommentsField" id="label2">Comments</label><p name="UNRcommentsField" id="UNRcommentsField" style="max-height:150px; overflow:auto;"> '+jQuery('#issue_actions_container .action-body').text()+'</p></div>');
			 
            // Add description
			jQuery('.jira-dialog-content .wiki-edit').parent().before('<div class="field-group toggle"><label for="UNRdescriptionField" id="label1">Description</label><p name="UNRdescriptionField" id="UNRdescriptionField" style="max-height:150px; overflow:auto;"> '+jQuery('#descriptionmodule .user-content-block').text()+'</p></div>');
			
            
            $("#UNRcommentsField").hide();
            
            $("#label2").click(function(){$("#UNRcommentsField").toggle();});
           
            
            UNRagressiveAskerToo();
		}
	}
	UNRagressiveAsker();
}

// engage!!
scriptLoader(mainStage);