// ==UserScript==
// @name		Sparkle Resource Links
// @namespace	atlassian.userscripts.SAC
// @include		https://support.atlassian.com/browse/*
// @author		Brad Mallow
// @description	This userscript adds useful links to Scout, Admin, & MAC relative to the context where the link appears. Current contexts include SEN, Reporter, and Company.
// ==/UserScript==

// inject the script into the page to take advantage of the already present` jQuery 
function scriptLoader(callback) {
	var script = document.createElement("script");
	script.textContent = "(" + callback.toString() + ")();";
	document.body.appendChild(script);
}

// the guts of this userscript, assuming we're on SAC
function centerStage() {

	// HSW (Hydra Scripts Wranglr) Version Info
	var currentVersion = 1.5;
	jQuery('body').append('<div class="HSWversionInfoTag" scriptname="Sparkle_Resource_Links" version="'+currentVersion+'" style="display: none;" />');

	// grab the reporter's email address
	jQuery('.HSWversionInfoTag:first').load(jQuery('#reporter-val a.user-hover').attr('href')+' #up-d-email', function() {
		var SRLcustomerEmail = jQuery('#up-d-email a').text();
		// insert buttons next to Customer
		jQuery('#reporter-val').prepend('<ul class="ops SRLbuttons Customer" style="display: inline-block;"><li><a title="Lookup in Admin" class="SRLadmin button first" href="https://admin.atlassian.com/UserBrowser.jspa?emailFilter='+SRLcustomerEmail+'" target="_blank">Admin</a></li><li><a title="Lookup in Scout" class="SRLscout button" href="https://scout.atlassian.com/search/for?id='+SRLcustomerEmail+'" target="_blank">Scout</a></li><li class="last"><a title="Lookup in MAC" class="SRLmac button last" href="https://my.atlassian.com/config/update?alternateUser='+SRLcustomerEmail+'" target="_blank">MAC</a></li></ul>');
	});

	// insert buttons next to SEN
	jQuery('strong[title="Support Entitlement Number (SEN)"]').next().addClass('SRLgetSEN');
	var SRLvalueSEN = jQuery('.SRLgetSEN').text().replace(/\s/g, '').replace('SEN-', '');
	if (SRLvalueSEN != 'UNKNOWN') jQuery('.SRLgetSEN').prepend('<ul class="ops SRLbuttons SEN" style="display: inline-block;"><li><a title="Lookup in Admin" class="SRLadmin button first" href="https://admin.atlassian.com/ViewAccount.jspa?accountId='+SRLvalueSEN+'" target="_blank">Admin</a></li><li class="last"><a title="Lookup in Scout" class="SRLscout button last" href="https://scout.atlassian.com/search/for?id='+SRLvalueSEN+'" target="_blank">Scout</a></li></ul>');

	// insert buttons next to Company
	jQuery('strong[title="Company"]').next().children('a:first').addClass('SRLgetCompany');
	var SRLvalueCompany = jQuery('.SRLgetCompany').text();
	jQuery('.SRLgetCompany').before('<ul class="ops SRLbuttons Company" style="display: inline-block;"><li><a title="Lookup in Admin" class="SRLadmin button first" href="https://admin.atlassian.com/UserBrowser.jspa?emailFilter='+SRLvalueCompany+'" target="_blank">Admin</a></li><li class="last"><a title="Lookup in Scout" class="SRLscout button last" href="https://scout.atlassian.com/search/for?id='+SRLvalueCompany+'" target="_blank">Scout</a></li></ul>');

	// move the SEN line above the Company line so it looks pretty
	jQuery('.SRLgetSEN').parent().parent().insertBefore(jQuery('.SRLgetCompany').parent().parent().parent());
}

// engage!!
scriptLoader(centerStage);