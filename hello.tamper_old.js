// ==UserScript==
// @name		hello
// @namespace	atlassian.userscripts.SAC
// @include		https://support.atlassian.com/browse/*
// @author		Osman Afridi
// @description	This userscript populates a 'hello {reporter first name} cheers {current logged in user} Atlassian Support - San Francisco' in bounce windows
// ==/UserScript==

// inject the script into the page to take advantage of the already present` jQuery 
function scriptLoader(callback) {
	var script = document.createElement("script");
	script.textContent = "(" + callback.toString() + ")();";
	document.body.appendChild(script);
}

// the guts of this userscript
function mainStage() {

	// HSW (Hydra Scripts Wranglr) Version Info
	var currentVersion = 1.2;
	jQuery('body').append('<div class="HSWversionInfoTag" scriptname="Unicorn_hello" version="'+currentVersion+'" style="display: none;" />');
	
	function UNRagressiveAsker() {
		if (!jQuery('.aui-popup-content .wiki-edit').length) {			
			setTimeout(UNRagressiveAsker, 500);
		} else {
			function UNRagressiveAskerToo() {
				if (jQuery('.aui-popup-content .wiki-edit').length) {			
					setTimeout(UNRagressiveAskerToo, 500);
				} else {
					UNRagressiveAsker();
				}
			}
            
            // Check if bounce screen 
            w = jQuery('.aui-popup-heading').text().trim();
            if(w.indexOf("Bounce") >= 0|| w.indexOf("Triage") >= 0) {
            
            // Take first name only
            x = jQuery('#reporter-val .user-hover').text();
            y = x.indexOf(" ");
            
            if (y > 0) {z = x.substring(0,y)}
            else z = x;
            
            // Capitalize first letter
            String.prototype.capitalize = function() {
             return this.charAt(0).toUpperCase() + this.slice(1);
            }
            
            z = z.capitalize();
            
			jQuery('#comment-wiki-edit #comment').append("Hi " + z + ",\n\n\n\nCheers,\n" + jQuery('#header-details-user-fullname').text().trim() + "\nAtlassian Support - San Francisco");
            
            }
            UNRagressiveAskerToo();
		}
	}
	UNRagressiveAsker();
}

// engage!!
scriptLoader(mainStage);